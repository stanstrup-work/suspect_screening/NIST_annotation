# NIST_extractor

## Tools
* nist_extractor.R: Extracts all the mass spectra from a NIST .L library.
* spectral_matching.R: Notes for R packages that do spectral matching.

## Examples
* examples/Peter.html
   * Walk-through of:
      * CAS --> Chemspider ID
      * Pull information from Chemspider
      * Extracting RI from the Chemspider info (no column etc. annotation)
      * **Pull RI information from NIST Webbook (*with* full column etc. annotation)**
   * Markdown to generate the html in Peter.Rmd
* examples/Nik.R
   * InChIKey --> Chemspider ID
   * Pull information from Chemspider
   * Extracting RI from the Chemspider info (no column etc. annotation)
   * **Get predicted properties from Chemspider**
* populating_suspect_list.R:
   * InChIKey --> Chemspider ID
   * Pull information from Chemspider
   * Extracting RI from the Chemspider info (no column etc. annotation)
   * Get predicted properties from Chemspider
   * **Get experimental properties from physprop. incl. Henry's constant**
   * **Match NIST spectra by CAS to suspect/target list**. See nist_extractor for how to get the NIST database exported.

## TODO
* Functions should be put in a package.



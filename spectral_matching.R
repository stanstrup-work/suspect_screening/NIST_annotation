# spectra matching options:
# * compareSpectra in MSnbase
# * spectralMatching in msPurity

# These packages contain full processing pipelines for GC including annotation with databases
# * eRah: deconvolution of GC–MS chromatograms. Seems to have something using the identifyComp function
# * decBaitmet in Baitmet
# * Auto.MS.MS.match.R in MetMSLine
# * metID.matchSpectralDB in compMS2Miner

# discussion of significance of matches: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3766387/

